// Paint example specifically for the TFTLCD breakout board.
// If using the Arduino shield, use the tftpaint_shield.pde sketch instead!
// DOES NOT CURRENTLY WORK ON ARDUINO LEONARDO

#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>
#include <AbsMouse.h>

#if defined(__SAM3X8E__)
#undef __FlashStringHelper::F(string_literal)
#define F(string_literal) string_literal
#endif


#ifndef OPTIMIZE
//USER
#define OPTIMIZE 1

#endif

#if OPTIMIZE
#define PRINT 0
#else

//USER
#define PRINT 0

#endif

#if PRINT
#include <stdarg.h>
void p(char *fmt, ...)
{
  char buf[256]; // resulting string limited to 128 chars
  va_list args;
  va_start(args, fmt);
  vsnprintf(buf, sizeof(buf), fmt, args);
  va_end(args);
  Serial.print(buf);
}
#else
#define p(...)
#endif

#define swap(type, a, b)    \
  do                        \
  {                         \
    register type _tmp = a; \
    a = b;                  \
    b = _tmp;               \
  } while (0);

#define BITSMAX(n) ((1 << (n)) - 1)

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7

// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).
//   D0 connects to digital pin 22
//   D1 connects to digital pin 23
//   D2 connects to digital pin 24
//   D3 connects to digital pin 25
//   D4 connects to digital pin 26
//   D5 connects to digital pin 27
//   D6 connects to digital pin 28
//   D7 connects to digital pin 29

// For the Arduino Due, use digital pins 33 through 40
// (on the 2-row header at the end of the board).
//   D0 connects to digital pin 33
//   D1 connects to digital pin 34
//   D2 connects to digital pin 35
//   D3 connects to digital pin 36
//   D4 connects to digital pin 37
//   D5 connects to digital pin 38
//   D6 connects to digital pin 39
//   D7 connects to digital pin 40

#define YP A3 // must be an analog pin, use "An" notation!
#define XM A2 // must be an analog pin, use "An" notation!
#define YM 9  // can be a digital pin
#define XP 8  // can be a digital pin

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 428);

//USER
#define MINPRESSURE 10
#define MAXPRESSURE 1000

#define touchingPressure(x) (MINPRESSURE < (x) && (x) < MAXPRESSURE)

inline bool isTouching()
{
  TSPoint ps = ts.getPoint();
  return touchingPressure(ps.z);
}

inline TSPoint pollTouch()
{
  TSPoint ps;
  do
  {
    ps = ts.getPoint();
  } while (!touchingPressure(ps.z));
  return ps;
}

#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
// optional
#define LCD_RESET A10

// Assign human-readable names to some common 16-bit color values:
#define BLACK 0x0000
#define BLUE 0x001F
#define RED 0xF800
#define GREEN 0x07E0
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define WHITE 0xFFFF

typedef union {
  struct
  {
    unsigned char B : 5;
    unsigned char G : 6;
    unsigned char R : 5;
  } __attribute__((packed)) bits;
  uint16_t word;
} __attribute__((packed)) color_t; // Little endian only!

#define COLMAX5 (BITSMAX(5))
#define COLMAX6 (BITSMAX(5))

Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

int TFTW, TFTH;

inline void switchTouch()
{
  digitalWrite(13, HIGH);
}

inline void switchDraw()
{
  digitalWrite(13, LOW);

  // if sharing pins, you'll need to fix the directions of the touchscreen pins
  //pinMode(XP, OUTPUT);
  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  //pinMode(YM, OUTPUT);
}

// ##### PLAY

#define SCALEPER 60

#define SCRW 1920
#define SCRH 1080

#define SCRhigherw ((SCRW) > (SCRH))
#define SCRhi ((SCRhigherw) ? (SCRW) : (SCRH))
#define SCRlo ((SCRhigherw) ? (SCRH) : (SCRW))

int PLX, PLXM, PLW;
int PLY, PLYM, PLH;
int32_t PLSCALE;
bool ROT90;

inline void calcPL()
{
  bool higherw = TFTW > TFTH;
  int hi = higherw ? TFTW : TFTH;
  int lo = higherw ? TFTH : TFTW;

  //p("hi=%d, lo=%d\n", hi, lo);

  ROT90 = SCRhigherw != higherw;

  int32_t scalehi = 1000ll * hi / SCRhi;
  int32_t scalelo = 1000ll * lo / SCRlo;

  PLSCALE = min(scalehi, scalelo) * SCALEPER / 100;

  //p("scalehi=%d, scalelo=%d, scale=%d\n", scalehi, scalelo, scale);

  int chi = hi / 2;
  int clo = lo / 2;

  int coffhi = SCRhi / 2 * PLSCALE / 1000;
  int cofflo = SCRlo / 2 * PLSCALE / 1000;

  //p("chi=%d, clo=%d, coffhi=%d, cofflo=%d\n", chi, clo, coffhi, cofflo);

  int hip = chi - coffhi;
  int him = chi + coffhi;
  int lop = clo - cofflo;
  int lom = clo + cofflo;

  //p("hip=%d, him=%d, lop=%d, lom=%d, lol=%d, hil=%d\n", hip, him, lop, lom, lol, hil);

  if (higherw)
  {
    PLX = hip;
    PLXM = him;

    PLY = lop;
    PLYM = lom;
  }
  else
  {
    PLX = lop;
    PLXM = lom;

    PLY = hip;
    PLYM = him;
  }

  PLW = PLXM - PLX;
  PLH = PLYM - PLY;
}

inline void drawRect()
{
  tft.drawRect(PLX, PLY, PLW, PLH, GREEN);
}

#define PARITIONEDREDRAW 1

#if PARITIONEDREDRAW

//USER
#define REDRAWPARTITIONX 1
#define REDRAWPARTITIONY 6

#if REDRAWPARTITIONX != 1 || REDRAWPARTITIONY != 1
struct
{
#if REDRAWPARTITIONX != 1
  int xi;
  int fracx;
#endif
#if REDRAWPARTITIONY != 1
  int yi;
  int fracy;
#endif
} rePa;
#endif

#if REDRAWPARTITIONX != 1 || REDRAWPARTITIONY != 1
inline void setupRedrawPartitions()
{
#if REDRAWPARTITIONX != 1
  rePa.fracx = (TFTW / REDRAWPARTITIONX) + 1;
#endif
#if REDRAWPARTITIONY != 1
  rePa.fracy = (TFTH / REDRAWPARTITIONY) + 1;
#endif
}
#else
#define setupRedrawPartitions(...)
#endif

inline bool redrawRectParitionedNext()
{
  register int x, w;
#if REDRAWPARTITIONX == 1
  x = 0;
  w = TFTW;
#else
  int xi = rePa.xi;
  {
    register int xir = xi;
    register int xf = rePa.fracx;
    x = xir * xf;
    w = xir == REDRAWPARTITIONX ? TFTW - x : xf;
  }
#endif

  register int y, h;
#if REDRAWPARTITIONY == 1
  y = 0;
  h = TFTH;
#else
  int yi = rePa.yi;
  {
    register int yir = yi;
    register int yf = rePa.fracy;
    y = yir * yf;
    h = yir == REDRAWPARTITIONY ? TFTH - y : yf;
  }
#endif

  tft.fillRect(x, y, w, h, BLACK);

#if REDRAWPARTITIONX != 1
  xi++;

  if (xi > REDRAWPARTITIONX)
  {
#if REDRAWPARTITIONY != 1
    yi++;
#endif
    rePa.xi = 0;
#if REDRAWPARTITIONY == 1
    return true;
#endif
  }
  else
  {
    rePa.xi = xi;
  }
#endif
#if REDRAWPARTITIONX == 1
  yi++;
#endif

#if REDRAWPARTITIONY != 1
  if (yi > REDRAWPARTITIONY)
  {
    rePa.yi = 0;
#if REDRAWPARTITIONX == 1
    return true;
#endif
  }
  else
  {
    rePa.yi = yi;
  }
#endif
#if REDRAWPARTITIONX == 1 && REDRAWPARTITIONY == 1
  return true;
#else
  return false;
#endif
}

#else
#define setupRedrawPartitions(...)
#define redrawRectParitionedNext(...)
#endif

#define TCFLIPX 0
#define TCFLIPY 1

#define CALIBCOLOR RED
#define TCCALIBRATED 1

#if TCCALIBRATED

#define calibrateTC()

// tc x=295 xm=745
// tc y=168 ym=798

#define TCX 334
#define TCXM 694
#define TCY 245
#define TCYM 731
#define TCW (TCXM - TCX)
#define TCH (TCYM - TCY)

#else

int TCX, TCXM, TCW;
int TCY, TCYM, TCH;

#define CALIBSCRAD (min(TFTW, TFTH) / 16)
#define CALIBCSIZE ((CALIBSCRAD)*2)

TSPoint calibrateTCPoint(int tftx, int tfty)
{
  tft.drawCircle(tftx, tfty, CALIBSCRAD, CALIBCOLOR);
  tft.drawFastHLine(tftx - CALIBSCRAD, tfty, CALIBCSIZE, CALIBCOLOR);
  tft.drawFastVLine(tftx, tfty - CALIBSCRAD, CALIBCSIZE, CALIBCOLOR);

  switchTouch();

  TSPoint tsp = pollTouch();

  while (isTouching())
  {
  }

  switchDraw();

  delay(300);

  return tsp;
}

inline void calibrateTC()
{
  TSPoint ll, hh; //xy
  ll = calibrateTCPoint(PLX, PLY);
  hh = calibrateTCPoint(PLXM, PLYM);

  TCX = ll.x;
  TCY = ll.y;
  TCXM = hh.x;
  TCYM = hh.y;

  p("TCPts x=%d\ty=%d\nTCPts x=%d\ty=%d\n", ll.x, ll.y, hh.x, hh.y);

  if (TCX > TCXM)
    swap(int, TCX, TCXM);

  if (TCY > TCYM)
    swap(int, TCY, TCYM);

  TCW = TCXM - TCX;
  TCH = TCYM - TCY;
}

#endif

#if OPTIMIZE
#define TRAIL 0
#else

//USER
#define TRAIL 1
#define TRAILINTERVAL 1
#define TRAILRADIUS 1
#define TRAILCOLINTERVAL 20

#endif

#if TRAIL

color_t trailcol;

struct
{
  uint8_t decColour;
  uint8_t crossfade;
  uint8_t ctr;
} tri;

inline void cycleRGB()
{
  if (tri.ctr < TRAILCOLINTERVAL)
  {
    tri.ctr++;
    return;
  }
  else
  {
    tri.ctr = 0;
  }

  p("w %u\tr %u\tg %u\tb %u\tdec %u\tcrs %u\n", trailcol.word, trailcol.bits.R, trailcol.bits.G, trailcol.bits.B, tri.decColour, tri.crossfade);

  switch (tri.decColour)
  {
  case 0: //dec r inc 1 G
    trailcol.bits.R--;
    trailcol.bits.G += 2;
    break;
  case 1: //dec G inc 2 b
    trailcol.bits.G -= 2;
    trailcol.bits.B++;
    break;
  case 2: //dec b inc 0 r
    trailcol.bits.B--;
    trailcol.bits.R++;
    break;
  }

  tri.crossfade++;

  if (tri.crossfade >= COLMAX5 / 2)
  {
    tri.crossfade = 0;
    tri.decColour++;
  }

  if (tri.decColour == 3)
  {
    tri.decColour = 0;
  }
}

//USER
#define REDRAWINTERVALABSOLUTE 1000

#define REDRAWINTERVAL ((REDRAWINTERVALABSOLUTE) / (TRAILINTERVAL))

#if REDRAWINTERVAL > 0
int redrawctr = 0;
#endif

#if TRAILINTERVAL > 0
int trailctr = 0;
#endif

inline void drawTrail(TSPoint ps)
{
#if TRAILINTERVAL > 1
  if (trailctr < TRAILINTERVAL)
  {
    trailctr++;
    return;
  }
  else
  {
    trailctr = 0;
  }
#endif

  int x = map(ps.x, TCX, TCXM, PLXM, PLX);
  int y = map(ps.y, TCY, TCYM, PLYM, PLY);

  switchDraw();

#if REDRAWINTERVAL > 0
  if (redrawctr < REDRAWINTERVAL)
  {
    redrawctr++;
  }
  else
  {
#if PARITIONEDREDRAW
    if (redrawRectParitionedNext())
      redrawctr = 0;
    else
    {
      drawRect();
      switchTouch();
      return;
    }
#else
    redrawRect();
    redrawctr = 0;
#endif
  }
#endif

#if TRAILRADIUS == 0
  tft.drawPixel(x, y, trailcol.word);
#elif TRAILRADIUS < 3
  tft.fillRect(x - TRAILRADIUS, y - TRAILRADIUS, TRAILRADIUS * 2, TRAILRADIUS * 2, trailcol.word);
#else
  tft.fillCircle(x, y, TRAILRADIUS, trailcol.word);
#endif
  cycleRGB();

  switchTouch();
}

inline void setupTrail()
{
  //trailcol.word = BITSMAX(16);
  trailcol.bits.R = COLMAX5 / 2;
}

#else
#define drawTrail(...)
#define setupTrail(...)
#endif

inline void setupPlay()
{
  AbsMouse.init(SCRW, SCRH);

  calcPL();

  calibrateTC();

  p("ROT90 %d\n", ROT90);
  p("PL x=%d, xm=%d, w=%d\nPL y=%d, ym=%d, h=%d\n", PLX, PLXM, PLW, PLY, PLYM, PLH);
  p("TC x=%d, xm=%d, w=%d\nTC y=%d, ym=%d, h=%d\n", TCX, TCXM, TCW, TCY, TCYM, TCH);

  setupTrail();
  setupRedrawPartitions();

  tft.fillScreen(BLACK);
  drawRect();

  switchTouch();

  while (!isTouching())
  {
  }
}

inline void drawPlay(bool touch, TSPoint ps)
{
  if (touch)
  {
    int scrx = ROT90 ? SCRH : SCRW;
    int scry = ROT90 ? SCRW : SCRH;

    int x = constrain(map(ps.x, TCX, TCXM, TCFLIPX ? scrx : 0, TCFLIPX ? 0 : scrx), 0, scrx);
    int y = constrain(map(ps.y, TCY, TCYM, TCFLIPY ? scry : 0, TCFLIPY ? 0 : scry), 0, scry);

    if (ROT90)
      swap(uint16_t, x, y);

    //p("psx = %d\tpsy = %d\tx = %d\ty = %d\n", ps.x, ps.y, x, y);
    AbsMouse.move(x, y);

    drawTrail(ps);
  }
}

void setup(void)
{
  Serial.begin(9600);
  //delay(500);
  Serial.println(F("Paint!"));

  tft.reset();

  uint16_t identifier = tft.readID();

  if (identifier == 0x9325)
  {
    Serial.println(F("Found ILI9325 LCD driver"));
  }
  else if (identifier == 0x9328)
  {
    Serial.println(F("Found ILI9328 LCD driver"));
  }
  else if (identifier == 0x7575)
  {
    Serial.println(F("Found HX8347G LCD driver"));
  }
  else if (identifier == 0x9341)
  {
    Serial.println(F("Found ILI9341 LCD driver"));
  }
  else if (identifier == 0x8357)
  {
    Serial.println(F("Found HX8357D LCD driver"));
  }
  else
  {
    Serial.print(F("Unknown LCD driver chip: "));
    Serial.println(identifier, HEX);
    Serial.println(F("If using the Adafruit 2.8\" TFT Arduino shield, the line:"));
    Serial.println(F("  #define USE_ADAFRUIT_SHIELD_PINOUT"));
    Serial.println(F("should appear in the library header (Adafruit_TFT.h)."));
    Serial.println(F("If using the breakout board, it should NOT be #defined!"));
    Serial.println(F("Also if using the breakout, double-check that all wiring"));
    Serial.println(F("matches the tutorial."));
    return;
  }

  tft.begin(identifier);

  TFTW = tft.width();
  TFTH = tft.height();

  tft.fillScreen(BLACK);

  //pinMode(13, OUTPUT);

  setupPlay();

  switchTouch();
}

void loop()
{
  TSPoint p = ts.getPoint();

  bool touch = touchingPressure(p.z);
  drawPlay(touch, p);
}